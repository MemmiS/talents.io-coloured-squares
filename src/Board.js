import { render } from '@testing-library/react';
import { useState } from 'react';
import './Board.css';
import Grid from './Grid';
const Board = () => {
    // const [squares, setSquares] = useState([])

    const [squares, setSquares] = useState(JSON.parse(localStorage.getItem('squares')) || [])
    const buttonsColor = ["red", "blue", "green", "yellow"]
    
    
    const clickHandler = (i) => {
        var array = squares
        array.push(buttonsColor[i])
        setSquares([...array.slice(-10)])
        localStorage.setItem('squares', JSON.stringify(squares))
    }

    return (
        <>
            <div className="fullboard">
                <button style={{backgroundColor: buttonsColor[0]}} className="button-top-left" type='button' onClick={() => clickHandler(0)}>colour1</button>
                <button className="button-top-right" style={{backgroundColor: buttonsColor[1]}} type='button' onClick={() => clickHandler(1)}>colour2</button>

                <Grid name={squares} />
                <button className="button-bottom-left" style={{backgroundColor: buttonsColor[2]}} type='button' onClick={() => clickHandler(2)}>colour3</button>
                <button className="button-bottom-right" style={{backgroundColor: buttonsColor[3]}}type='button' onClick={() => clickHandler(3)}>colour4</button>
            
            </div>
         </>
    )
};

export default Board;