import { useState } from "react"

const Grid = (props) => {
    return (
        <ul className="grid" style={{ padding: '20%', display:'flex', justifyContent: 'center', alignItems: 'center', listStyleType: 'none'}}>
        
        {props.name.map((color, index) => (
            <li key={index} style={{ width: '40px', height: '40px', backgroundColor: color}}></li>
        ))}
        
        
    </ul>
    )
}

export default Grid;